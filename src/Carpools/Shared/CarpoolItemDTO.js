/**

 Copyright (c) 2018, MOBICOOP. All rights reserved.
 This project is dual licensed under AGPL and proprietary licence.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program. If not, see <gnu.org/licenses>.

 Licence MOBICOOP described in the file
 LICENSE
 **************************/

export default class CarpoolItemDTO {

  construct(data) {

  }

  carpoolItemFromSearch(carpool) {
    this.id = carpool.id;
    this.frequency = carpool.frequency;
    this.price = carpool.roundedPrice;
    this.passenger = !!carpool.resultDriver;
    this.driver = !!carpool.resultPassenger;
    this.seats = carpool.seats;
    this.origin = carpool.origin;
    this.destination = carpool.destination;
    this.carpooler = this.getCarpooler(carpool);
    if (carpool.communities) {
      this.community = Array.isArray(carpool.communities) ? carpool.communities : Object.keys(carpool.communities);
    }
    this.pendingAsk = carpool.pendingAsk;
    this.acceptedAsk = carpool.acceptedAsk;
    if (carpool.frequency == 1) {
      this.date = carpool.date;
      this.time = carpool.time;
      // indexTime permet d'afficher l'horaire de prise en charge, si passager on prend le 2eme outward waypoint et l'avant dernier pour la dépose.
      const indexTime = !!carpool.resultDriver ? 1 : 0;
      this.outwardTime = this.resultDriveOrPassenger(carpool).outward.waypoints[indexTime].time;
      const arr = [...this.resultDriveOrPassenger(carpool).outward.waypoints];
      this.outwardEndTime = arr[arr.length - (1 + indexTime)].time;
    }
    if (carpool.frequency == 2) {
      this.regularDays = this.getRegularDaysFromSearch(carpool);
      this.outwardTime = carpool.outwardTime;
      this.returnTime = carpool.returnTime;
    }
    this.resultDriverOrPassenger = this.resultDriveOrPassenger(carpool);
    if (this.resultDriverOrPassenger && this.resultDriverOrPassenger.outward) {
      this.isMultipleTimes = this.resultDriverOrPassenger.outward.multipleTimes;
    }
    this.paymentStatus = carpool.paymentStatus
    return this;
  }

  carpoolItemFromRdex(carpool) {
    this.id = carpool.id;
    this.externalOperator = carpool.externalOperator;
    this.externalOrigin = carpool.externalOrigin;
    this.externalUrl = carpool.externalUrl;
    this.frequency = carpool.frequency;
    this.price = carpool.roundedPrice;
    this.passenger = !!carpool.resultDriver;
    this.driver = !!carpool.resultPassenger;
    this.seats = carpool.seats;
    this.origin = carpool.origin;
    this.destination = carpool.destination;
    this.carpooler = this.getCarpooler(carpool);
    this.pendingAsk = carpool.pendingAsk;
    this.acceptedAsk = carpool.acceptedAsk;
    if (carpool.frequency == 1) {
      this.date = carpool.date;
      this.time = carpool.time;
      // indexTime permet d'afficher l'horaire de prise en charge, si passager on prend le 2eme outward waypoint et l'avant dernier pour la dépose.
      this.outwardTime = carpool.time;
      // this.outwardEndTime = null
    }
    if (carpool.frequency == 2) {
      this.regularDays = this.getRegularDaysFromSearch(carpool);
      this.outwardTime = carpool.outwardTime;
      this.returnTime = carpool.returnTime;
    }
    this.resultDriverOrPassenger = {
      outward: {
        originDriver: carpool.origin,
        originPassenger: carpool.origin,
        destinationDriver: carpool.destination,
        destinationPassenger: carpool.destination,
        multipleTimes: false
      }
    }
    if (this.resultDriverOrPassenger && this.resultDriverOrPassenger.outward) {
      this.isMultipleTimes = this.resultDriverOrPassenger.outward.multipleTimes;
    }
    return this;
  }

  carpoolItemFromMyCarpool(carpool) {
    this.id = carpool.id;
    this.frequency = carpool.frequency;
    this.passenger = carpool.rolePassenger;
    this.driver = carpool.roleDriver;
    this.origin = carpool.waypoints[0];
    this.destination = (carpool.waypoints.filter(element => element.destination === true))[0];
    this.isMultipleTimes = false;
    this.paused = carpool.paused;
    this.proposalId = carpool.id;
    this.asks = carpool.asks;
    this.potentialCarpoolers = carpool.carpoolers;
    if (carpool.frequency == 1) {
      this.date = carpool.outwardDate;
      this.time = carpool.outwardTime;
      this.outwardTime = carpool.outwardTime;
      this.returnTime = carpool.returnTime;
    }
    if (carpool.frequency == 2) {
      this.dateValidity = carpool.toDate;
      this.regularDays = this.getRegularDaysFromMyCarpool(carpool.schedule);
      this.outwardTime = carpool.schedule.outwardTime;
      this.returnTime = carpool.schedule.returnTime;
    }
    return this;
  }

  carpoolItemFromMyAcceptedCarpool(carpool, passengerIndex) {
    passengerIndex = passengerIndex || 0
    this.id = carpool.id;
    this.frequency = carpool.roleDriver && carpool.passengers[passengerIndex] ? carpool.passengers[passengerIndex].askFrequency : carpool.driver.askFrequency;
    this.price = carpool.roleDriver && carpool.passengers[passengerIndex] ? carpool.passengers[passengerIndex].price : carpool.driver.price;
    this.passenger = carpool.rolePassenger && carpool.driver;
    this.driver = carpool.roleDriver && carpool.passengers[passengerIndex];
    this.seats = carpool.seats;
    this.origin = carpool.waypoints[0];
    this.destination = (carpool.waypoints.filter(element => element.destination === true))[0];
    this.carpooler = carpool.roleDriver && carpool.passengers[passengerIndex] ? carpool.passengers[passengerIndex] : carpool.driver;
    if (carpool.communities) {
      this.community = Array.isArray(carpool.communities) ? carpool.communities : Object.keys(carpool.communities);
    }
    if (carpool.frequency == 1) {
      this.date = carpool.outwardDate;
      this.time = carpool.outwardTime;
      if (carpool.roleDriver && carpool.rolePassenger) {
        this.outwardTime = carpool.passengers.length > 0 ? carpool.passengers[passengerIndex].pickUpTime : carpool.driver.pickUpTime;
        this.outwardEndTime = carpool.passengers.length > 0 ? carpool.passengers[passengerIndex].dropOffTime : carpool.driver.dropOffTime;
      } else {
        this.outwardTime = carpool.roleDriver && carpool.passengers[passengerIndex] ? carpool.passengers[passengerIndex].pickUpTime : carpool.driver.pickUpTime;
        this.outwardEndTime = carpool.roleDriver && carpool.passengers[passengerIndex] ? carpool.passengers[passengerIndex].dropOffTime : carpool.driver.dropOffTime;
      }
    }
    if (carpool.frequency == 2) {
      this.regularDays = carpool.rolePassenger ? this.getRegularDaysFromMyCarpool(carpool.driver.schedule) : this.getRegularDaysFromMyCarpool(carpool.passengers[passengerIndex].schedule);
      this.outwardTime = carpool.schedule.outwardTime;
      this.returnTime = carpool.schedule.returnTime;
    }
    this.paymentStatus = carpool.paymentStatus
    return this;
  }

  carpoolItemFromAsk(ask) {
    const carpool = ask.results[0];
    this.id = carpool.id;
    this.frequency = carpool.frequency;
    this.carpooler = carpool.carpooler;
    this.originAddress = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'origin' && item.person === 'requester').address
    this.pickUpAddress = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'origin' && item.person === 'carpooler').address
    this.dropOffAddress = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'destination' && item.person === 'carpooler').address
    this.destinationAddress = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'destination' && item.person === 'requester').address
    this.originTime = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'origin' && item.person === 'requester').time
    this.pickUpTime = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'origin' && item.person === 'carpooler').time
    this.dropOffTime = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'destination' && item.person === 'carpooler').time
    this.destinationTime = this.resultDriveOrPassenger(carpool).outward.waypoints.find(item => item.type === 'destination' && item.person === 'requester').time
    this.distance = this.resultDriveOrPassenger(carpool).outward.commonDistance + this.resultDriveOrPassenger(carpool).outward.detourDistance
    this.passenger = !!carpool.resultDriver;
    this.driver = !!carpool.resultPassenger;
    this.seats = carpool.seats;
    this.price = carpool.roundedPrice;
    this.return = carpool.return;
    this.status = ask.askStatus;
    this.canUpdateAsk = ask.canUpdateAsk;
    if (carpool.frequency == 2) {
      this.regularDays = this.getRegularDaysFromSearch(carpool);
      this.outwardTime = this.getTimes(this.resultDriveOrPassenger(carpool).outward);
      this.returnTime = this.getTimes(this.resultDriveOrPassenger(carpool).return);
    }
    return this;
  }

  getCarpooler(carpool) {
    const carpooler = {};
    if (carpool.carpooler.avatars) {
      carpooler.avatar = carpool.carpooler.avatars[0];
    }
    carpooler.id = carpool.carpooler.id;
    carpooler.givenName = carpool.carpooler.givenName;
    carpooler.shortFamilyName = carpool.carpooler.shortFamilyName;
    carpooler.telephone = carpool.carpooler.telephone;
    carpooler.phoneDisplay = carpool.carpooler.phoneDisplay;
    carpooler.experienced = carpool.carpooler.experienced;
    carpooler.numberOfBadges = carpool.carpooler.numberOfBadges;
    carpooler.verifiedIdentity = carpool.carpooler.verifiedIdentity;
    return carpooler
  }

  getRegularDaysFromSearch(carpool) {
    const result = [];
    if (carpool) {
      result.push({ trad: 'Carpool.L', value: carpool.monCheck });
      result.push({ trad: 'Carpool.Ma', value: carpool.tueCheck });
      result.push({ trad: 'Carpool.Me', value: carpool.wedCheck });
      result.push({ trad: 'Carpool.J', value: carpool.thuCheck });
      result.push({ trad: 'Carpool.V', value: carpool.friCheck });
      result.push({ trad: 'Carpool.S', value: carpool.satCheck });
      result.push({ trad: 'Carpool.D', value: carpool.sunCheck });
    }
    return result;
  }

  getRegularDaysFromMyCarpool(schedule) {
    const result = [];
    if (schedule) {
      result.push({ trad: 'Carpool.L', value: schedule.mon.check });
      result.push({ trad: 'Carpool.Ma', value: schedule.tue.check });
      result.push({ trad: 'Carpool.Me', value: schedule.wed.check });
      result.push({ trad: 'Carpool.J', value: schedule.thu.check });
      result.push({ trad: 'Carpool.V', value: schedule.fri.check });
      result.push({ trad: 'Carpool.S', value: schedule.sat.check });
      result.push({ trad: 'Carpool.D', value: schedule.sun.check });
    }
    return result;
  }

  getTimes(carpool) {
    const result = [];
    if (carpool) {
      result.push({ trad: 'Carpool.L', value: carpool.monTime });
      result.push({ trad: 'Carpool.Ma', value: carpool.tueTime });
      result.push({ trad: 'Carpool.Me', value: carpool.wedTime });
      result.push({ trad: 'Carpool.J', value: carpool.thuTime });
      result.push({ trad: 'Carpool.V', value: carpool.friTime });
      result.push({ trad: 'Carpool.S', value: carpool.satTime });
      result.push({ trad: 'Carpool.D', value: carpool.sunTime });
    }
    return result;
  }

  resultDriveOrPassenger(carpool) {
    return !!carpool.resultDriver ? carpool.resultDriver : carpool.resultPassenger;
  }

}
