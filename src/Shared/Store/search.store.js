/**

 Copyright (c) 2018, MOBICOOP. All rights reserved.
 This project is dual licensed under AGPL and proprietary licence.

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU Affero General Public License for more details.
 You should have received a copy of the GNU Affero General Public License
 along with this program. If not, see <gnu.org/licenses>.

 Licence MOBICOOP described in the file
 LICENSE
 **************************/

import http from '../Mixin/http.mixin'
import moment from 'moment'
import { address } from "../../Shared/Mixin/address.mixin";

export const searchStore = {
  state: {
    statusSearch: '',
    statusSearchPage: '',
    statusGeo: '',
    statusCoordinate: '',
    resultSearch: [],
    resultExternalAd: null,
    resultsGeo: [],
    previousSearch: [],
    display: {
      origin: '',
      destination: '',
      outwardDate: ''
    },
    searchObject: {
      search: true,
      role: 2,
      frequency: !!process.env.VUE_APP_DEFAULT_CARPOOL_FREQUENCY ? parseInt(process.env.VUE_APP_DEFAULT_CARPOOL_FREQUENCY) : 1,
      outwardWaypoints: [],
      outwardDate: new Date(),
      userId: null,
      adId: null
    },
    page: 1,
    resultSize: 0,
    adId: 0,
    filters: null,
    savedSearchId: null
  },
  mutations: {
    geo_request(state) {
      state.statusGeo = 'loading';
    },

    geo_coordinate_request(state) {
      state.statusCoordinate = 'loading';
    },

    geo_succes(state, resultGeo) {
      const groups = [
        { group:"locality", points: []},
        { group:"street", points: []},
        { group:"housenumber", points: []},
        { group:"user", points: []},
        { group:"venue", points: []},
        { group:"event", points: []},
        { group:"relaypoint", points: []}
      ]

      resultGeo.forEach(item => {
        item.displayLabel = [address.methods.propositionTitle(item), address.methods.propositionText(item), address.methods.propositionTemplate(item)]
        const group = groups.find(g => g.group === item.type)
        if (group) {
          group.points.push(item);
        }
      })
      state.statusGeo = 'success';
      state.resultsGeo = groups;
    },

    geo_coordinate_success(state, resultGeo) {
      state.statusCoordinnate = 'success';
    },

    geo_error(state) {
      state.statusGeo = 'error';
      state.resultGeo = [];
    },

    geo_coordinate_error(state) {
      state.statusCoordinate = 'error';
    },

    set_filters(state, filters) {
      state.filters = filters;
    },

    search_request(state) {
      state.statusSearch = 'loading';
      state.page = 1;
      state.resultSize = 0;
      state.resultSearch = [];
    },

    search_succes(state, resultSearch) {
      state.statusSearch = 'success';
      if (resultSearch.length > 0) {
        state.adId = resultSearch[0].id
      }
      state.resultSearch = resultSearch;
    },

    save_result_ad(state, resultAd) {
      state.resultExternalAd = resultAd
    },

    rdex_to_search(state, data) {
      state.searchObject.search = true;
      state.searchObject.role = 3;
      state.searchObject.frequency= data.frequency;
      state.searchObject.outwardWaypoints= data.outwardWaypoints.map(item => item.address);
      state.searchObject.outwardDate = !!data.results[0] ? data.results[0].date : null;
      state.searchObject.userId = null;
      state.searchObject.adId = null;
      state.display.origin = address.methods.formatAddress(data.outwardWaypoints[0].address);
      state.display.destination = address.methods.formatAddress(data.outwardWaypoints[1].address);
    },

    search_error(state) {
      state.statusSearch = 'error';
      state.resultSearch = [];
    },

    search_page_request(state) {
      state.statusSearchPage = 'loading';
    },

    search_page_success(state, resultSearch) {
      state.statusSearchPage = 'success';
      state.resultSearch.push(...resultSearch);
    },

    search_page_error(state) {
      state.statusSearch = 'error';
    },

    set_result_size(state, size) {
      state.resultSize = size;
    },

    changeUserIdOfSearch(state, userId) {
      state.searchObject.userId = userId
    },

    changeOrigin(state, payload) {
      state.searchObject.outwardWaypoints[0] = payload.addressDTO;
      state.display.origin = payload.displayGeo;
    },

    changeDestination(state, payload) {
      state.searchObject.outwardWaypoints[1] = payload.addressDTO;
      state.display.destination = payload.displayGeo;
    },

    changePreviousSearch(state, previousSearch) {
      state.previousSearch = previousSearch;
    },

    reset_search_object(state) {
      state.searchObject.adId = null
    },

    save_search_id(state, id) {
      state.savedSearchId = id;
    }

  },
  actions: {

    /**
     * Fonction qui retourne des addresses en fonction d'un string
     */
    geoSearch: ({ commit }, params) => {
      commit('geo_request')
      return new Promise((resolve, reject) => {
        http.get("/points?search=" + params.searchQuery).then(resp => {
          if (resp) {
            commit('geo_succes', resp.data["hydra:member"])
            resolve(resp)
          }
        }).catch(err => {
          commit('geo_error')
          reject(err)
        })
      })
    },

    /**
     * Fonction qui retourne des addresses en fonction d'une latitude, longitude
     */
    getAddressesByCoordinate: ({ commit }, params) => {
      commit('geo_coordinate_request');
      return new Promise((resolve, reject) => {
        http.get("/addresses/reverse?latitude=" +  params.latitude + "&longitude=" + params.longitude).then(resp => {
          if (resp) {
            commit('geo_coordinate_success', resp.data["hydra:member"]);
            resolve(resp)
          }
        }).catch(err => {
          commit('geo_coordinate_error');
          reject(err)
        })
      })
    },

    /**
     * Fonction qui intervetit l'origine et la desitnation
     */
    swapDestinationAndOrigin({ commit, state }) {
      state.searchObject.outwardWaypoints.reverse();
      const newOrigin = state.display.destination;
      const newDestination = state.display.origin;

      state.display.destination = newDestination;
      state.display.origin = newOrigin;
    },

    /**
     * Fonction qui effectue la recherche
     */
    searchCarpools({ commit, getters }, filters) {
      commit('search_request')
      return new Promise((resolve, reject) => {

        const data = Object.assign({}, getters.searchObject);
        if (data.frequency == 2) {
          delete data.outwardDate;
        }

        if (!!filters && !!filters.communities) {
          data.communities = filters.communities
        }

        if (!!filters && !!filters.rdex) {
          http.get("/carpools/" + filters.rdex + "/external").then(resp => {
            if (resp) {
              commit('rdex_to_search', resp.data);
              commit('set_result_size', resp.data["results"].length);
              commit('search_succes', resp.data["results"]);
              commit('save_result_ad', resp.data);
              commit('save_search_id', resp.data.id);
              resolve(resp)
            }
          }).catch(err => {
            commit('search_error')
            reject(err)
          })
        } else if (data.adId) {
          http.get("/carpools/" + data.adId, data).then(resp => {
            if (resp) {
              commit('set_result_size', resp.data["results"].length);
              commit('search_succes', resp.data["results"]);
              commit('save_result_ad', null);
              commit('save_search_id', resp.data.id);
              resolve(resp)
            }
          }).catch(err => {
            commit('search_error')
            reject(err)
          })
        } else {
          http.post( "/carpools", data).then(resp => {
            if (resp) {
              commit('set_result_size', resp.data.nbResults);
              commit('search_succes', resp.data["results"]);
              commit('save_result_ad', null);
              commit('save_search_id', resp.data.id);
              resolve(resp)
            }
          }).catch(err => {
            commit('search_error');
            reject(err)
          })
        }

      })
    },

    // Claim
    claimProposal: ({commit}, id) => {
      return new Promise((resolve, reject) => {
        http.put("/carpools/" + id + "/claim", {}).then(resp => {
          if (resp) {
            resolve(resp)
          }
        }).catch(err => {
          reject(err)
        })
      })
    },

    /**
     * Fonction qui change la page
     */
    searchChangePage({ commit, getters }, filters) {
      commit('search_page_request')
      return new Promise((resolve, reject) => {

        const data = Object.assign({}, getters.searchObject);
        if (data.frequency == 2) {
          delete data.outwardDate;
        }

        if (!!filters && !!filters.communities) {
          data.communities = filters.communities
        }

          http.get("/carpools/" + getters.adId + '?page=' + getters.page, data).then(resp => {
            if (resp) {
              commit('search_page_success', resp.data["results"]);
              resolve(resp)
            }
          }).catch(err => {
            commit('search_page_error');
            reject(err)
          })
      })
    },

    getDirectPointSearch({ }, payload) {

      let query = '';
      payload.addresses.forEach((add, index, array) => {
        query += `points[${index}][longitude]=${add.longitude}&points[${index}][latitude]=${add.latitude}`;
        if (index !== array.length - 1) {
          query += '&';
        }
      });

      return new Promise((resolve, reject) => {
        return http.get(`/directions/search?${query}`)
          .then(resp => {
            resolve(resp)
          })
          .catch(err => {
            console.log(err)
            reject(err)
          })
      })
    },

    setPreviousSearch({ commit, getters }, address) {
      let previousSearch = getters.previousSearch;

      previousSearch = previousSearch.filter(item => !!item.displayLabel[2] && !!item.displayLabel[2].icon);
      previousSearch.unshift(address);

      previousSearch = previousSearch.slice(0, 3)

      // previousSearch.push(address);
      commit('changePreviousSearch', previousSearch)
    },

    checkOutWardDate({commit, state}) {
      let date = moment(state.searchObject.outwardDate);
      const todayDate = moment();
      if (date.isBefore(todayDate)) {
        state.searchObject.outwardDate = new Date();
      }
    }
  },

  getters: {
    page: state => {
      return state.page;
    },

    adId: state => {
      return state.adId;
    },

    resultSize: state => {
      return state.resultSize;
    },

    searchOrigin: state => {
      return state.searchObject.outwardWaypoints[0];
    },

    searchDestination: state => {
      return state.searchObject.outwardWaypoints[1];
    },

    displayOrigin: state => {
      return state.display.origin;
    },

    displayDestination: state => {
      return state.display.destination;
    },

    searchObject: state => {
      return state.searchObject;
    },

    resultSearch: state => {
      return state.resultSearch;
    },

    statusSearch: state => {
      return state.statusSearch;
    },

    previousSearch: state => {
      return state.previousSearch;
    }
  }
}

export default { searchStore };
