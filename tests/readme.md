# Run test

```
POST=... EMAIL=... PASSWORD=... URL=... npx playwright test tests/**",
```
POST=on for action who save data in webservice (optional if not set some tests are skipped)
EMAIL is the email of the user you want authenticated (optional if not set some tests are skipped)
PASSWORD is the password of the user you want authenticated (optional if not set some tests are skipped)
URL is the base url for example http://localhost:8080/ or https://m.test.mobicoop.io/ (mandatory)