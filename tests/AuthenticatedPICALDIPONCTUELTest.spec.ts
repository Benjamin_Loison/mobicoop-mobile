import { expect } from '@playwright/test';
import { test } from '../fixtures.js';
import { Utils } from './utils';

const EMAIL = process.env.EMAILPICALDI;
const PASSWORD = process.env.PASSWORDPICALDI;
const EMAILLABEL = 'EMAILPICALDI';
const PASSWORDLABEL = 'PASSWORDPICALDI';

const EMAILPONCTUEL = process.env.EMAILPONCTUEL;
const PASSWORDPONCTUEL = process.env.PASSWORDPONCTUEL;
const EMAILLABELPONCTUEL = 'EMAILPONCTUEL';
const PASSWORDLABELPONCTUEL = 'PASSWORDPONCTUEL';

test.beforeEach(async ({ page }) => {

    await Utils.connect(EMAIL, PASSWORD, EMAILLABEL, PASSWORDLABEL, page, test);

});

test.afterEach(async ({ page }) => {

    await Utils.disconnect(EMAILPONCTUEL, PASSWORDPONCTUEL, EMAILLABELPONCTUEL, PASSWORDLABELPONCTUEL, page, test, expect);

});

test('RC1.5 - Contacter un usager dans le cadre d’un covoiturage (depuis une recherche)', async ({ page }) => {

    test.skip(process.env.POST !== 'on', 'need POST=on to enable this test');

    await page.locator('#originButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=origin&action=search');

    await page.locator('input[name="search"]').fill('Nancy, 54');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    // wait for 1 second
    await page.waitForTimeout(1000);

    await page.locator('#destinationButton').click();
    await expect(page).toHaveURL(process.env.URL + '#/carpools/geosearch?type=destination&action=search');

    await page.locator('input[name="search"]').fill('Metz, 57');

    await page.locator('#result >> nth=0').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/home');

    await page.locator('ion-checkbox').click();

    //    await page.waitForTimeout(1000);

    await page.locator('#searchButton').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/search');

    const firstResponse = page.locator('.mc-carpool-item >> nth=0');

    const d = new Date();

    const dateLocator = d.toLocaleDateString('fr-FR').slice(0, 5).replace('/', '.');

    await expect(firstResponse.locator('span', { hasText: dateLocator })).toBeVisible();

    await expect(firstResponse.locator('text=0.7 €')).toBeVisible();

    await expect(firstResponse.locator('text=07/40')).toBeVisible();

    await expect(firstResponse.locator('.mc-carpool-carpooler')).toHaveText(/Cora R./);

    await firstResponse.click();

    await expect(page).toHaveURL(process.env.URL + '#/carpool-detail/0');

    await page.locator('#contactButton').click();

    await expect(page).toHaveURL(process.env.URL + '#/carpools/message/');

    const text = 'Colin, oh mon miroir, mon doux miroir, qui est le meilleur covoitureur de tous ?'

    await page.locator('.bottom-message >> textarea >> nth=-1').fill(text);

    await page.locator('.day-message-right >> nth=-1 >> has-text("' + text + '"');

    await page.locator('ion-header >> button').click();

    await page.locator('ion-header >> button').click();

    await page.locator('ion-header >> button').click();

    await page.locator('name="logout"').click();

    await Utils.connect(EMAILPONCTUEL, PASSWORDPONCTUEL, EMAILLABELPONCTUEL, PASSWORDLABELPONCTUEL, page, test);

    await page.locator('[id="tab-button-messages"] >> ion-badge').isVisible();

    await page.locator('[id="tab-button-messages"]').click();


});

async function setTime(page, targetTime, nth) {
    while (true) {
        await page.waitForTimeout(150);

        let currentStartTime = await page.locator('.picker-opt-selected >> nth=' + nth).textContent();
        const compare = currentStartTime.localeCompare(targetTime);
        if (compare == 0) {
            break;
        } else if (compare > 0) {
            await page.locator('button:above(:nth-match(.picker-opt-selected,' + (nth + 1) + '))').first().click();
        } else {
            await page.locator('button:below(:nth-match(.picker-opt-selected,' + (nth + 1) + '))').first().click();
        }
    }

}
