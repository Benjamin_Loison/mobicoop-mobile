export class Utils {

    public static async connect(EMAIL: string, PASSWORD: string, EMAILLABEL: string, PASSWORDLABEL: string, page, test) {
        test.skip(EMAIL === undefined || PASSWORD === undefined, 'need ' + EMAILLABEL + ' and ' + PASSWORDLABEL + ' to connect this test');

        await page.goto(process.env.URL);

        await page.locator('[id="tab-button-help"]').click();

        await page.locator('[id="tab-button-register"]').click();

        await page.locator('[id="tab-button-login"]').click();

        await page.locator('[id="tab-button-home"]').click();

        const loginButton = page.locator('id=signinButton');
        await loginButton.click();

        await page.locator('[type="email"] >> nth=-1').fill(EMAIL);

        await page.locator('[type="password"] >> nth=-1').fill(PASSWORD);

        await page.locator('[id="loginButton"]').click();

        if (await page.locator('[id="cookieButton"]') !== null) {
            await page.locator('[id="cookieButton"]').click();
        }
    }

    public static async disconnect(EMAIL: string, PASSWORD: string, EMAILLABEL: string, PASSWORDLABEL: string, page, test, expect) {
        if(test.info().status === 'skipped'){
            return;
        }
        test.skip(EMAIL === undefined || PASSWORD === undefined, 'need ' + EMAILLABEL + ' and ' + PASSWORDLABEL + ' to disconnect this test');

        await page.locator('[id="tab-button-profile"]').click();

        await page.locator('#logoutButton').click();

        await expect(page).toHaveURL(process.env.URL + '#/carpools/home');
    }

}
