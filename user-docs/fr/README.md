# Documentation fonctionnelle de Mobicoop Mobile
### _version française_

> :construction:
> **Cette documentation est un travail en cours.**
>
> Elle sera complétée par 2 processus parallèles :
>
> 1. la réponse à des questions fonctionnelles posées au Responsable Produits.
> 1. la conception fonctionnelle de nouvelles évolutions.
>
> :construction:

La documentation est organisée par écran du client Mobile de la plateforme.

## Écrans dont la documentation a été initiée:

- [Connexion](carpools/login-fr.md) :construction:


## Conventions mises en oeuvre dans cette documentation

- Hormis cette page d'accueil de la documentation fonctionnelle nommé `README.md` de manière standard, chaque fichier reprend le chemin de son URL dans le client mobile en version anglaise ajouté du suffixe `-fr` si c'est la version française.