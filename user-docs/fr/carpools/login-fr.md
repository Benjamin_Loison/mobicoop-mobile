_ [Documentation fonctionnelle](../README.md) / Covoiturage / Connexion_
# Connexion


_**Documentation en cours**_ :construction:

[[_TOC_]]

## Autres informations

**Durée d'une session**

Au bout de 30 jours d'inactivité, la session est coupée.

_Techniquement:_ Cette durée de session est définie de manière centralisée par l'API comme durée maximale donnée au `refreshToken`.