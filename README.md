# mobicoop-v2

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# launch an instance on the next available local port (usually 8084)
# check the launch log to see it
You need to find the right command in the package.json in the scripts section.
i.e. to launch the mobicoop version its :

npm run serve:mobicoop

# build for production with minification
npm run build
```

**Attention** : If you want to use your local mobicoop-platform API, check if there is a local version of the command in the package.json
i.e for mobicoop with local API you've got :
``` bash
npm run serve:mobicoop:local
```
In your database, you'll need to have the default `app` in the app table for the mobile. Username `mobile`, password `mobile`, and the corresponding `app_auth_item` with the `auth_item_id` set to 5.

## Mobicoop Instance Build Setup

``` bash
# On build l'instance, elle se génère dans le dossier www
npm run build:[instance-name]

# Copie www dans le dossier public dans le projet Android ou iOS
npx cap copy [platform]

# Ouvre le projet dans Android Studio ou XCode
npx cap open [platform]

# Pour XCode sélectionner le bon scheme puis procédure normale ios

# Pour Android Studio sélectionner le bon Build Variant puis procédure normale android
```

## Create Mobicoop Instance

``` bash

# Créer un fichier environnemnt .env.[instance-name]

# Mettre à jour le fichier package.json avec les commandes serve:[instance-name] et build:[instance-name]

# Dans le projet iOS :
    * Créer un nouveau scheme portant le nom de [instance-name] (Possibilité de dupliquer un ancien scheme)
    * Renommer le fichier -Info.plist créé automatiquement en [instance-name]-Info.plist et modifier ses Target Membership
    * Préciser le nom [instance-name]-Info.plist dans Build Settings -> Info plist File
    * Créer un fichier XCAssets [instance-name]Assets.xcassets et modifier ses Target Membership
    * Ajouter logo et splash dans le fichier XCAssets
    * Ajouter le scheme control script dans Build Phases
    * Ajouter la target dans le Podfile et faire npx cap sync ios

# Dans le projet Android :
    * Ajouter un productFlavors dans le fichier build.gradle (Module: app)
    * Créer un dossier [instance-name] au même niveau que le dossier main
    * Ajouter à l'intérieur une copy des fichier res avec les bons logo et splash screen et en modifiant le fichier strings.xml

```


## Icon & Splash
```
$ cordova-res ios --skip-config --copy
$ cordova-res android --skip-config --copy
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


## Create Deeplink for instance
https://capacitorjs.com/docs/guides/deep-links
